import { ConfirmationDialogComponent } from './../Dialog/confirmation-dialog/confirmation-dialog.component';
import { OperationalService } from './../../Services/operational.service';
import { DataService } from './../../Services/data.service';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CreateUpdateProductDialogComponent } from '../Dialog/create-update-product-dialog/create-update-product-dialog.component';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  //myarray: ProductDetails[];
  productGroup: FormGroup;
  isUpdate: boolean;
  updateIndex: number = -99;
  searchValue: number;

  constructor(private fb: FormBuilder, public dialog: MatDialog, public dataService: DataService, private operationalService: OperationalService) {
  }

  getProducts: Subscription;

  ngOnInit(): void {
    this.GetProductList();
  }

  /**
   * Method for Open Dialog
   */
  DeleteProduct(index) {
    //this.dataService.myarray.splice(index, 1);
    const confirmationDialogComponent = this.dialog.open(ConfirmationDialogComponent, {
      maxWidth: '380px',
      maxHeight: '300px',
      disableClose: true,
      panelClass: 'no-padding-dialog'
    });

    console.log("Dialog Data", confirmationDialogComponent);

    confirmationDialogComponent.afterClosed().subscribe(confirmation => {
      if (confirmation) {
        this.dataService.myarray.splice(index, 1);
        this.operationalService.DeleteProductFromList(index);
      }
    })
  }

  /**
   * Method for Open Dialog
   */
  EditProduct(index) {
    this.updateIndex = index;
    this.isUpdate = true;
    this.OpenDialog();
  }

  /**
  * Method for Open Dialog
  */
  NewProduct() {
    this.updateIndex = -99;
    this.OpenDialog();
  }

  /**
   * Method for Open Dialog
   */
  OpenDialog() {
    const calsDfChEvaluationDialogRef = this.dialog.open(CreateUpdateProductDialogComponent, {
      maxWidth: '500px',
      maxHeight: '350px',
      disableClose: true,
      panelClass: 'no-padding-dialog',
      data: this.updateIndex
    });
  }

  /**
   * Search Product from list
   */
  SearchProduct() {
    if (this.searchValue != undefined && this.searchValue != null) {
      let tempArray = this.dataService.myarray.filter(x => x.id == this.searchValue);
      this.dataService.myarray = tempArray;
    }
    else {
      this.GetProductList();
    }
  }

  /**
   * Method to get Product List from API (JSON Server)
   */
  GetProductList() {
    this.getProducts = this.operationalService.GetProductList().subscribe((result: any) => {
      //this.subGetBasicProjectDetails = this.operationalService.GetBasicProjectDetails(false).subscribe(result => {
      if (result === null || result === undefined) {
        this.dataService.myarray = [];
      } else {
        this.dataService.myarray = result.products;
      }
    }, error => {
      console.log('Error', 'Error in receiving products.');
    });

  }

  /**
   * On destroy unsubscribe the observable
   */
  ngOnDestroy() {
    if (this.getProducts) {
      if (!this.getProducts.closed) {
        this.getProducts.unsubscribe();
      }
    }
  }
}
