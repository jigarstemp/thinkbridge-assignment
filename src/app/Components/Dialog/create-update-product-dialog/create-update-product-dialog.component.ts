import { OperationalService } from './../../../Services/operational.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataService } from 'src/app/Services/data.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Inject } from '@angular/core';

@Component({
  selector: 'app-create-update-product-dialog',
  templateUrl: './create-update-product-dialog.component.html',
  styleUrls: ['./create-update-product-dialog.component.scss']
})
export class CreateUpdateProductDialogComponent implements OnInit {
  isUpdate: boolean = false;
  productGroup: FormGroup;

  constructor(private fb: FormBuilder, public dataService: DataService, @Inject(MAT_DIALOG_DATA) public data: any, private operationalService: OperationalService) {
    this.CreateFormGroup();
  }

  ngOnInit(): void {
    if (this.data >= 0) {
      this.isUpdate = true;
      this.productGroup.setValue(this.dataService.myarray[this.data]);
    }
  }

  /**
  * Method to create FormGroup
  */
  CreateFormGroup() {
    this.productGroup = this.fb.group({
      id: [{ value: '', disabled: this.isUpdate }, [Validators.required]],
      productDesc: [{ value: '', disabled: this.isUpdate }, Validators.required],
      productQty: [{ value: '', disabled: this.isUpdate }, Validators.required],
      productPrice: [{ value: '', disabled: this.isUpdate }, Validators.required]
    });
  }

  /**
   * Method to add New Product in array
   */
  AddNewProduct() {
    this.isUpdate = false;
    this.dataService.myarray.push(this.productGroup.value);
  }

  /**
   * Method to update product list
   */
  UpdateProduct() {
    if (this.data != undefined && this.data != null) {
      this.dataService.myarray[this.data].id = this.productGroup.get('id').value;
      this.dataService.myarray[this.data].productDesc = this.productGroup.get('productDesc').value;
      this.dataService.myarray[this.data].productQty = this.productGroup.get('productQty').value;
      this.dataService.myarray[this.data].productPrice = this.productGroup.get('productPrice').value;

      // let jsonObject = Object.assign(this.dataService.myarray.map(key => Object.values(key)).map(value => ({ [value[0]]: value[1] })));
      // let json = JSON.stringify(jsonObject);
      // console.log(this.dataService.myarray);
      // console.log(json);

      // this.operationalService.UpdateProductList(json);
    }
  }
}