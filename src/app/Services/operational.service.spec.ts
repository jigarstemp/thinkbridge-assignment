import { TestBed } from '@angular/core/testing';

import { OperationalService } from './operational.service';

describe('OperationalService', () => {
  let service: OperationalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OperationalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
