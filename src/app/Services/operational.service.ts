import { ProductDetails } from './../Models/ProductDetails';
import { Environment } from './../Helper/Environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})

export class OperationalService {

  
  constructor(private http: HttpClient) { }

  /**
   * Get product List from API
   */
  public GetProductList() {
    return this.http.get<ProductDetails[]>('./assets/ProductList.json')
      .pipe(catchError(this.handleError)
      );
  }

  /**
   * Update product in List using API
   */
  public UpdateProductList(json: string) {

    return this.http.put(Environment.OPERATIONAL_API_URL_LOCAL, json)
      .pipe(catchError(this.handleError)
      );
  }

  /**
   * Delete product from List using API
   */
  public DeleteProductFromList(id: string) {
    return this.http.put(Environment.OPERATIONAL_API_URL_LOCAL, id)
      .pipe(catchError(this.handleError)
      );
  }

  /**
   * Add product from List using API
   */
   public AddProductFromList(data: ProductDetails) {
    return this.http.put(Environment.OPERATIONAL_API_URL_LOCAL, data)
      .pipe(catchError(this.handleError)
      );
  }

  // Error handling
  handleError(error) {
    console.log(error);
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      const apiUrl: string[] = error.url.split('/');
      const apiMethod = apiUrl.length <= 0 ? '' : apiUrl[apiUrl.length - 1];
      errorMessage = `Error Code: (${error.error.errorCode})\nMessage:${apiMethod}- ${error.error.message}`;
    }
    return throwError(errorMessage);
  }
}
