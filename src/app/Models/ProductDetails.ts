export class ProductDetails {
  public id: number;
  public productDesc: string;
  public productQty: number;
  public productPrice: number;
}